﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QuanLySachApi.Models
{
    public interface ISachRepository
    {
        List<Sach> GetAll();
        Sach GetById(int id);
        Sach Add(Sach s);
        bool Update(Sach s);
        bool Delete(int id);

    }

}
