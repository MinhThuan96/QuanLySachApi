﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace QuanLySachApi.Models
{
    public class SachRepository : ISachRepository
    {
        private List<Sach> _sach = new List<Sach>();
        private int id = 0; // Id tự tăng

        public SachRepository()
        {
            //Title, Content, AuthorName, Price
            this.Add(new Sach() { Title = "Nghệ Thuật Bán Mình cho Sếp", AuthorName = "Orison Swett Marden", Price = 55000 });
            this.Add(new Sach() { Title = "Gã Nghiện Giày - Tự Truyện Của Nhà Sáng Lập NIKE", AuthorName = "Phil Knight", Price = 120000 });
            this.Add(new Sach() { Title = "Chạm Tới Giấc Mơ (Phiên Bản Bìa Mềm)", AuthorName = "Sơn Tùng M-TP", Price = 88000 });

        }

        public List<Sach> GetAll()
        {
            return _sach;
        }

        public Sach GetById(int id)
        {
            return _sach.FirstOrDefault(x => x.Id == id);
        }

        public Sach Add(Sach s)
        {
            s.Id = id++;
            _sach.Add(s);
            return s;
        }

        public bool Update(Sach s)
        {
            int id = _sach.FindIndex(x => x.Id == s.Id);
            if (id == -1)
            {
                return false;
            }
            else
            {
                _sach.RemoveAt(id);
                _sach.Add(s);
                return true;
            }
        }

        public bool Delete(int id)
        {
            Sach s = _sach.FirstOrDefault(x => x.Id == id);
            if (s == null)
            {
                return false;
            }
            else
            {
                _sach.Remove(s);
                return true;
            }
        }

    }
}