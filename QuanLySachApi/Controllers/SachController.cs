﻿using QuanLySachApi.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace QuanLySachApi.Controllers
{
    public class SachController : ApiController
    {
        SachRepository _sach = new SachRepository();

        //Lay danh sach cac sach
        public List<Sach> GetAll()
        {
            return _sach.GetAll();
        }

        //Lay sach theo ma
        public Sach GetById(int id)
        {
            return _sach.GetById(id);
        }

        //Them moi sach
        public HttpResponseMessage PostSach(Sach s)
        {
            s = _sach.Add(s);
            //HttpStatusCode.Created: là mã trạng thái 201: Yêu cầu đã thành công và các máy chủ tạo ra một nguồn tài nguyên mới.
            var repose = Request.CreateResponse<Sach>(HttpStatusCode.Created, s);
            string uri = Url.Link("DefaultApi", new { id = s.Id });
            repose.Headers.Location = new Uri(uri);
            return repose;
        }


        //Cap nhat cho sach
        public void PutSach(int id, Sach s)
        {
            s.Id = id;
            if (!_sach.Update(s))
            {
                //Return về mã trạng thái là 404: các trang yêu cầu không tồn tại
                throw new HttpResponseException(HttpStatusCode.NotFound);
            }
        }

        //Xoa sach
        public HttpResponseMessage Delete(int id)
        {
            if (_sach.Delete(id))
            {
                //Return (HttpStatusCode.NoContent) về mã trạng thái là 204: Các máy chủ xử lý yêu cầu thành công, nhưng không trả lại bất kỳ nội dung nào
                return new HttpResponseMessage(HttpStatusCode.NoContent);
            }
            else
            {
                //Return về mã trạng thái là 404: các trang yêu cầu không tồn tại
                return new HttpResponseMessage(HttpStatusCode.NotFound);
            }
        }
    }
}
